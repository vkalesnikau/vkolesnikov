public class StringValidator implements Validator<String> {
    @Override
    public boolean validate(String input) {
        String firstLetter = input.substring(0, 1);
        if (firstLetter.matches("[A-Z]")) {
            return true;
        } else {
            throw new ValidationFailedException("String first character was not upper case!");
        }
    }
}
