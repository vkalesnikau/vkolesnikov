import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public final class Serializator {
    private Serializator() {

    }

    public static void serialize(Object obj, String pathToFile) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(pathToFile);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(obj);
            objectOutputStream.close();
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public static Object deserialize(String pathToFile) {
        try {
            FileInputStream fileInputStream = new FileInputStream(pathToFile);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            Object deserialized = objectInputStream.readObject();
            objectInputStream.close();
            return deserialized;
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        throw new RuntimeException("Unsuccessful deserialization");
    }
}
