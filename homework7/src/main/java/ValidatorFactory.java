import java.util.HashMap;

public final class ValidatorFactory {
    private ValidatorFactory() {
    }

    private static HashMap<String, Validator> validators = createMap();

    private static HashMap<String, Validator> createMap() {
        HashMap<String, Validator> map = new HashMap<>();
        map.put("Integer", new IntegerValidator());
        map.put("String", new StringValidator());
        return map;
    }

    public static Validator getValidator(String key) {
        return validators.get(key);
    }
}