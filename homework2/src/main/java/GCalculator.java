import java.util.Scanner;

/*
 * This class allows to find G
 * @Author: Valentin Kolesnikov
 * @Version: 1.0
 */
public class GCalculator {
    public static void main(String[] args) {
        showInputMessage();
        Scanner consoleInput = new Scanner(System.in);
        try {
            String[] parameters = consoleInput.nextLine().split(" ");
            if (parameters.length != 4) {
                throw new Exception("Entered number of numbers is not equal to 4");
            }
            int a = Integer.parseInt(parameters[0]);
            int p = Integer.parseInt(parameters[1]);
            double m1 = Double.parseDouble(parameters[2]);
            double m2 = Double.parseDouble(parameters[3]);

            double g = calculateG(a, p, m1, m2);

            showCalculatedG(g);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /*This method helps to calculate G
     * @param a - number
     * @param p - number
     * @param m1 - number
     * @param m1 - number
     * @return calculated number G
     * */
    public static double calculateG(int a, int p, double m1, double m2) {
        return 4 * Math.pow(Math.PI, 2) * (Math.pow(a, 3) / (Math.pow(p, 2) * m1 * m2));
    }
    /*This method show input message in console*/
    public static void showInputMessage() {
        System.out.println("Enter parameters a, p, m1, m2 through white spaces");
    }
    /*This method show calculated number G in console*/
    public static void showCalculatedG(double g) {
        System.out.printf("G = " + g);
    }
}