import java.util.ArrayList;
import java.util.Collections;

public class WordGroup implements Comparable<WordGroup> {

    public WordGroup(String letter) {
        this.letter = letter.toUpperCase();
        occurrences = new ArrayList<>();
    }

    public String getLetter() {
        return letter;
    }

    private String letter;

    public ArrayList<Occurrence> getOccurrences() {
        return occurrences;
    }

    private ArrayList<Occurrence> occurrences;

    private void addWord(String word) {
        if (word != null) {
            occurrences.add(new Occurrence(word));
        } else {
            throw new IllegalArgumentException(
                    "Adding occurrence was null"
            );
        }
    }

    public Occurrence getOccurrence(String word) {
        for (Occurrence occurrence : occurrences
        ) {
            if (occurrence.getWord().equals(word)) {
                return occurrence;
            }
        }
        return null;
    }

    public void distribute(String word) {
        Occurrence occurrence = this.getOccurrence(word);
        if (occurrence != null) {
            String firstLetter = occurrence.getWord().substring(0, 1);
            if (firstLetter.equalsIgnoreCase(letter)) {
                occurrence.setAmount(occurrence.getAmount() + 1);
            } else {
                throw new IllegalArgumentException("Word can't be in this group");
            }
        } else {
            this.addWord(word);
        }
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder("\n" + letter + ":\n\t");
        for (Occurrence occurrence : occurrences
        ) {
            string.append(occurrence.toString() + "\n\t");
        }
        int size = string.length();
        return string.toString().substring(0, size - 1);
    }

    @Override
    public int hashCode() {
        return letter.hashCode() + occurrences.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        WordGroup wordGroup = (WordGroup) obj;
        String letter = wordGroup.getLetter();
        ArrayList<Occurrence> occurrences = wordGroup.getOccurrences();
        return (this.letter.equals(letter)
                && this.occurrences.equals(occurrences));
    }

    @Override
    public int compareTo(WordGroup other) {
        return this.letter.compareTo(other.getLetter());
    }
}
