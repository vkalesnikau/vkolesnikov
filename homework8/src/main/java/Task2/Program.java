package Task2;

import java.util.ArrayList;
import java.util.Arrays;

public class Program {
    public static void main(String[] args) {
        Worker carpenter = new Worker(new ArrayList(Arrays.asList(Skill.CARPENTER)));
        Worker mason = new Worker(new ArrayList(Arrays.asList(Skill.MASON)));
        Worker electrician = new Worker(
                new ArrayList(Arrays.asList(Skill.ELECTRICIAN)));
        Worker masonCarpenter = new Worker(
                new ArrayList(Arrays.asList(Skill.CARPENTER, Skill.MASON)));
        Worker masonElectrician = new Worker(
                new ArrayList(Arrays.asList(Skill.MASON, Skill.ELECTRICIAN)));
        Worker carpenterElectrician = new Worker(
                new ArrayList(Arrays.asList(Skill.CARPENTER, Skill.ELECTRICIAN)));


        Brigade required = new Brigade(
                new ArrayList(Arrays.asList(carpenter, mason, mason, electrician)),
                300);

        Brigade brigade1 = new Brigade(
                new ArrayList(Arrays.asList(carpenter, mason, electrician)),
                100);
        Brigade brigade2 = new Brigade(
                new ArrayList(Arrays.asList(masonCarpenter, masonElectrician)),
                200);
        Brigade brigade3 = new Brigade(
                new ArrayList(Arrays.asList(carpenterElectrician, electrician, mason, mason)),
                300);
        ArrayList<Brigade> brigades = new ArrayList<>(Arrays.asList(brigade1, brigade2, brigade3));

        ConstructionProject constructionProject = new ConstructionProject(required);


        Brigade foundBrigade = constructionProject.getSuitableBrigade(brigades);


        if (foundBrigade != null) {
            System.out.println("Suitable brigade was found! This brigade is");
            System.out.println(foundBrigade);
        } else {
            System.out.println("There are no suitable brigades, project is closed");
        }


    }
}
