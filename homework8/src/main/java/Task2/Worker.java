package Task2;

import java.util.ArrayList;

public class Worker {

    public Worker(ArrayList<Skill> skills) {
        this.skills = skills;
    }

    public ArrayList<Skill> skills;

    public boolean equalsOrBetter(Worker other) {
        for (Skill skill : other.skills) {
            if (!this.hasThisSkill(skill)) {
                return false;
            }
        }
        return true;
    }

    public boolean hasThisSkill(Skill currentSkill) {
        for (Skill skill : skills) {
            if (skill == currentSkill) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder workerString = new StringBuilder();
        for (Skill skill : skills) {
            workerString.append(skill + " ");
        }
        return workerString.toString();
    }
}