import java.util.ArrayList;
import java.util.Arrays;

public final class TextEditor {

    private TextEditor() {

    }

    public static ArrayList<String> convertToList(String text) {
        String[] array = text.split(" ");
        ArrayList<String> words = new ArrayList<>(Arrays.asList(array));
        return words;
    }

    public static String deletePunctuation(String text) {
        String regex = "[^A-Za-z0-9 ]";
        return text.replaceAll(regex, "");
    }

}