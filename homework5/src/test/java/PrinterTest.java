import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PrinterTest {

    private Printer printer = new Printer();
    private PathParser parser = new PathParser();
    private Folder root = new Folder("root");
    private StructureCreator creator = new StructureCreator(parser, root);



    @Before
    public void createStructure() {
        parser.setPath("root/folder1");
        creator.create();
        parser.setPath("root/folder1/folder3/test.txt");
        creator.create();
    }

    @Test
    public void testPrint() {
        String assertString = "root/\n\tfolder1/\n\t\tfolder3/\n\t\t\ttest.txt\n";
        assertEquals(assertString, printer.print(root).toString());
    }
}
