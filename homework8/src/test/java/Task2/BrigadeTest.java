package Task2;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class BrigadeTest {
    private Worker carpenter = new Worker(new ArrayList(Arrays.asList(Skill.CARPENTER)));
    private Worker mason = new Worker(new ArrayList(Arrays.asList(Skill.MASON)));
    private Worker electrician = new Worker(new ArrayList(Arrays.asList(Skill.ELECTRICIAN)));
    private Brigade brigade1 = new Brigade(new ArrayList(Arrays.asList(carpenter, mason, electrician)), 100);
    private Brigade brigade2 = new Brigade(new ArrayList(Arrays.asList(carpenter, mason, electrician)), 200);
    private Brigade brigade3 = new Brigade(new ArrayList(Arrays.asList(carpenter, mason)), 50);

    @Test
    public void testEqualsOrBetterFalseHasNoNecessaryWorkers() {
        Assert.assertFalse(brigade3.equalsOrBetter(brigade1));
    }

    @Test
    public void testEqualsOrBetterFalseExpensive() {
        Assert.assertFalse(brigade2.equalsOrBetter(brigade1));
    }

    @Test
    public void testEqualsOrBetterTrue() {
        Assert.assertTrue(brigade1.equalsOrBetter(brigade2));
    }

}