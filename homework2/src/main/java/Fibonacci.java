import java.util.ArrayList;
/*
 * This class allows to find the Fibonacci series of a given length using different loops types
 * @Author: Valentin Kolesnikov
 * @Version: 1.0
 */
public class Fibonacci {
    /*Field containing the length of the fibonacci series*/
    private int numberCount;

    /*Constructor - create new object*/
    public Fibonacci(int numberCount) {
        this.numberCount = numberCount;
    }

    /*This method helps to calculate Fibonacci number using loop for
     * @return array of Fibonacci numbers
     * */
    public ArrayList<Integer> getFibNumbersByForLoop() {
        ArrayList<Integer> fibNumbers = new ArrayList<Integer>();
        int n0 = 0;
        int n1 = 1;
        int n2;

        fibNumbers.add(n0);
        fibNumbers.add(n1);

        for (int i = 2; i < this.numberCount; i++) {
            n2 = n0 + n1;
            n0 = n1;
            n1 = n2;
            fibNumbers.add(n2);
        }

        return fibNumbers;
    }

    /*This method helps to calculate Fibonacci number using loop while
     * @return array of Fibonacci numbers
     * */
    public ArrayList<Integer> getFibNumbersByWhileLoop() {
        ArrayList<Integer> fibNumbers = new ArrayList<Integer>();
        int n0 = 0;
        int n1 = 1;
        int n2;
        int i = 2;
        fibNumbers.add(n0);
        fibNumbers.add(n1);

        while (i < this.numberCount) {
            n2 = n0 + n1;
            n0 = n1;
            n1 = n2;
            i++;
            fibNumbers.add(n2);

        }

        return fibNumbers;
    }

    /*This method helps to calculate Fibonacci number using loop do-while
     * @return array of Fibonacci numbers
     * */
    public ArrayList<Integer> getFibNumbersByDoWhileLoop() {
        ArrayList<Integer> fibNumbers = new ArrayList<Integer>();
        int n0 = 0;
        int n1 = 1;
        int n2;
        int i = 2;
        fibNumbers.add(n0);
        fibNumbers.add(n1);

        do {
            if (this.numberCount != 0) {
                n2 = n0 + n1;
                n0 = n1;
                n1 = n2;
                fibNumbers.add(n2);
                i++;
            }
        }
        while (i < this.numberCount);

        return fibNumbers;
    }
}
