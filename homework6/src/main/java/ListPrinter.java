import java.util.ArrayList;
import java.util.Arrays;

public final class ListPrinter {
    private ListPrinter() {
    }

    public static <T> String print(ArrayList<T> list) {
        String string = Arrays.toString(list.toArray());
        return string.replaceAll("[\\[\\],]", "");
    }

}