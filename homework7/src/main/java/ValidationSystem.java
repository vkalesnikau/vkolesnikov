public final class ValidationSystem {

    private ValidationSystem() {

    }

    public static <T> boolean validate(T input) {
        if (nullOrEmpty(input)) {
            throw new ValidationFailedException("Validation string can't be null or empty!");
        }
        String key = input.getClass().getSimpleName();
        Validator validator = ValidatorFactory.getValidator(key);
        return validator.validate(input);
    }

    private static <T> boolean nullOrEmpty(T input) {
        if (input == null || input == "") {
            return true;
        } else {
            return false;
        }
    }


}