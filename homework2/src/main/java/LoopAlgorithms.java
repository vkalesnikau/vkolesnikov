import java.util.ArrayList;
import java.util.Scanner;

/*
 * This class calculate the Fibonacci series of a given length and the factorial of a number using different loops types
 * @Author: Valentin Kolesnikov
 * @Version: 1.0
 */
public class LoopAlgorithms {
    public static void main(String[] args) {
        showInputMessage();
        Scanner input = new Scanner(System.in);
        try {
            String[] parameters = input.nextLine().split(" ");
            if (parameters.length != 3) {
                throw new Exception("Entered number of numbers is not equal to 3");
            }
            int algorithmType = Integer.parseInt(parameters[0]);
            int loopType = Integer.parseInt(parameters[1]);
            int parameter = Integer.parseInt(parameters[2]);

            if (algorithmType == 1) {
                Fibonacci fibonacci = new Fibonacci(parameter);
                ArrayList<Integer> fibNumbers;
                if (loopType == 1) {
                    fibNumbers = fibonacci.getFibNumbersByWhileLoop();
                    showFibonacciNumbers(fibNumbers);
                } else if (loopType == 2) {
                    fibNumbers = fibonacci.getFibNumbersByDoWhileLoop();
                    showFibonacciNumbers(fibNumbers);
                } else if (loopType == 3) {
                    fibNumbers = fibonacci.getFibNumbersByForLoop();
                    showFibonacciNumbers(fibNumbers);
                } else {
                    showErrorMessage();
                }
            } else if (algorithmType == 2) {
                Factorial factorial = new Factorial(parameter);
                int result;
                if (loopType == 1) {
                    result = factorial.getFactorialByWhileLoop();
                    showFactorial(result);
                } else if (loopType == 2) {
                    result = factorial.getFactorialByDoWhileLoop();
                    showFactorial(result);
                } else if (loopType == 3) {
                    result = factorial.getFactorialByForLoop();
                    showFactorial(result);
                } else {
                    showErrorMessage();
                }
            } else {
                showErrorMessage();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*This method show input message in console*/
    public static void showInputMessage() {
        System.out.println("Enter type of algorithm, type of loop and parameter through white space");
    }

    /*This method show Fibonacci numbers in console*/
    public static void showFibonacciNumbers(ArrayList<Integer> fibNumbers) {
        System.out.println("Fibonacci series: ");
        for (int i = 0; i < fibNumbers.toArray().length; i++) {
            System.out.println(fibNumbers.get(i) + " ");
        }
    }

    /*This method show factorial of number in console*/
    public static void showFactorial(int result) {
        System.out.println("Factorial: " + result);
    }

    /*This method show error message in console*/
    public static void showErrorMessage() {
        System.out.println("Entered data is wrong");
    }
}

