package FirstTask;

import org.junit.Assert;
import org.junit.Test;

public class DigitSumComparatorTest {

    private DigitSumComparator digitSumComparator = new DigitSumComparator();

    @Test
    public void testCompare() {
        Assert.assertEquals(-1, digitSumComparator.compare(12, 22));
        Assert.assertEquals(0, digitSumComparator.compare(12, 12));
        Assert.assertEquals(1, digitSumComparator.compare(22, 12));
        Assert.assertEquals(0, digitSumComparator.compare(-12, 12));
    }
}