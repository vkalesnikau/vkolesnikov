package Task2;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class ConstructionProjectTest {
    private Worker carpenter = new Worker(new ArrayList(Arrays.asList(Skill.CARPENTER)));
    private Worker mason = new Worker(new ArrayList(Arrays.asList(Skill.MASON)));
    private Worker electrician = new Worker(new ArrayList(Arrays.asList(Skill.ELECTRICIAN)));

    private Brigade required = new Brigade(new ArrayList(Arrays.asList(carpenter, mason, electrician)),
            300);

    private Brigade brigade2 = new Brigade(new ArrayList(Arrays.asList(carpenter, mason, electrician)),
            210);
    private Brigade brigade3 = new Brigade(new ArrayList(Arrays.asList(carpenter, mason, electrician)),
            200);
    private Brigade brigade4 = new Brigade(new ArrayList(Arrays.asList(carpenter, mason)),
            50);

    ArrayList<Brigade> brigades = new ArrayList<>(Arrays.asList(brigade2, brigade3, brigade4));
    private ConstructionProject constructionProject = new ConstructionProject(required);

    @Test
    public void testGetSuitableBrigade() {
        constructionProject.getSuitableBrigade(brigades);
    }


}