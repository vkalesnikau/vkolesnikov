import java.util.ArrayList;

public final class Distributor {

    private Distributor() {

    }

    public static ArrayList<WordGroup> groupByFirstLetter(ArrayList<String> words) {
        ArrayList<WordGroup> groups = new ArrayList<>();
        for (String currentWord : words
        ) {
            String firstLetter = currentWord.substring(0, 1);
            WordGroup group = getGroupByLetter(groups, firstLetter);
            if (group != null) {
                group.distribute(currentWord);
            } else {
                addWordToNewGroup(currentWord, groups);
            }
        }
        return groups;
    }

    private static WordGroup getGroupByLetter(ArrayList<WordGroup> groups, String letter) {
        for (WordGroup group : groups
        ) {
            if (group.getLetter().equalsIgnoreCase(letter)) {
                return group;
            }
        }
        return null;
    }

    private static void addWordToNewGroup(String word, ArrayList<WordGroup> groups) {
        String firstLetter = word.substring(0, 1);
        WordGroup newGroup = new WordGroup(firstLetter);
        newGroup.distribute(word);
        groups.add(newGroup);
    }

}
