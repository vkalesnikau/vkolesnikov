package Task;

import Task.card.Card;
import Task.card.CreditCard;
import Task.card.DebitCard;

import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.Scanner;

public class AtmUi {
    public static void run(String[] args) {
        CreditCard creditCard = new CreditCard("Valentin Kolesnikov");
        DebitCard debitCard = new DebitCard("Valentin Kolesnikov");
        CashMachine atm = new CashMachine();
        atm.setCapacity(BigDecimal.valueOf(30000));
        atm.setMoneyAmount(BigDecimal.valueOf(10000));
        selectAction(atm, creditCard);
    }

    static void printSelectionInstructions() {
        System.out.println("Please, enter number to select action\n"
                + "1 - to put money on\n"
                + "2 - to withdraw money\n"
                + "3 - to exit"
        );
    }

    static boolean checkValidInputNumber(double input, double max, double min) {
        return input >= min && input <= max;
    }

    static int getInputNumber(double min, double max) {
        Scanner scanner = new Scanner(System.in);
        int number = 0;
        try {
            number = scanner.nextInt();
            while (!checkValidInputNumber(number, max, min)) {
                System.out.println("Uncorrect input, please"
                        + " enter number from " + min + " to " + max);
                number = scanner.nextInt();
            }
        } catch (InputMismatchException e) {
            System.out.println("Uncorrect input, it seems like you entered not number ");
        }
        return number;
    }

    public static void selectAction(CashMachine atm, Card card) {
        int action = 0;
        while (action != 3) {
            Scanner scanner = new Scanner(System.in);
            printSelectionInstructions();
            if (scanner.hasNextInt()) {
                action = scanner.nextInt();
            } else {
                action = 0;
            }
            switch (action) {
                case 1:
                    replenishAccount(atm, card);
                    break;
                case 2:
                    withdrawMoney(atm, card);
                    break;
                case 3:
                    break;
                default:
                    System.out.println("Uncorrect input, please enter number 1-3");
            }
        }
    }


    static void printReplenishmentInstructions(CashMachine atm) {
        System.out.println("Please deposit requred sum.\n"
                + "Attention, You can deposite no more than "
                + atm.getDifference() + " BYN at a time.");
    }

    static void replenishAccount(CashMachine atm, Card card) {
        printReplenishmentInstructions(atm);
        BigDecimal sum = BigDecimal.valueOf(getInputNumber(0, atm.getDifference().doubleValue()));
        try {
            atm.replenishAccount(card, sum);
            System.out.println("Balance was replenished by " + sum + " BYN.");
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
    }

    static void printWithdrawalInstructions(CashMachine atm) {
        System.out.println("Please enter required sum.\n"
                + "Attention, You can withdraw no more than "
                + atm.getMoneyAmount() + " BYN at a time.");
    }

    static void withdrawMoney(CashMachine atm, Card card) {
        printWithdrawalInstructions(atm);
        BigDecimal sum = BigDecimal.valueOf(getInputNumber(0, atm.getMoneyAmount().doubleValue()));
        try {
            atm.withdrawMoney(card, sum);
            System.out.println(sum + " BYN withdrawn from account");
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
    }

}
