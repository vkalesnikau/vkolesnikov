package FirstTask;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class DigitSumSorterTest {

    @Test
    public void testSort() {
        Integer[] array = new Integer[]{14, 17, 21};
        Integer[] array2 = new Integer[]{-12, 19, 23};
        Integer[] expected = new Integer[]{21, 14, 17};
        Integer[] expected2 = new Integer[]{-12, 23, 19};
        DigitSumSorter.sort(array);
        DigitSumSorter.sort(array2);
        Assert.assertEquals(expected, array);
        Assert.assertEquals(expected2, array2);
    }
}