package Task.runnables;

import Task.CashMachine;
import Task.card.Card;


import java.math.BigDecimal;

public class WithdrawalRunnable extends AtmRunnable {

    private final BigDecimal WITHDRAWAL_SUM = new BigDecimal(10);
    private final int SLEEP_TIME = 5000;

    public WithdrawalRunnable(CashMachine atm, Card card, String atmName) {
        super(atm, card, atmName);
    }

    @Override
    protected void printChangeBalanceMessage() {
        System.out.println("Money was withdrawn by " + atmName
                + ", balance = " + card.getBalance() + "$");
    }


    @Override
    public void run() {
        while (isBalanceBetweenLimits()) {
            atm.withdrawMoney(card, WITHDRAWAL_SUM);
            printChangeBalanceMessage();
            putThreadToSleep(SLEEP_TIME);
        }
        printFinishMessage();
    }
}

