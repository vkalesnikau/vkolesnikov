package Task.card;

import java.math.BigDecimal;

public class CreditCard extends Card {
    public CreditCard(String ownerName, BigDecimal balance) {
        if (ownerName == null) {
            throw new NullPointerException("Please enter owner name");
        } else if (balance == null) {
            throw new NullPointerException("Please enter balance");
        } else {
            this.ownerName = ownerName;
            this.balance = balance;
        }
    }

    public CreditCard(String ownerName) {
        this(ownerName, BigDecimal.ZERO);
    }

    @Override
    public boolean withdraw(BigDecimal sum) {
        if (sum.signum() == -1) {
            throw new IllegalArgumentException("Sum can't be negative");
        } else {
            balance = balance.subtract(sum);
            return true;
        }
    }
}
