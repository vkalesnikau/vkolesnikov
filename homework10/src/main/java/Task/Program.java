package Task;

import Task.card.CreditCard;
import Task.runnables.ReplenishmentRunnable;
import Task.runnables.WithdrawalRunnable;


import java.math.BigDecimal;
import java.util.ArrayList;

public class Program {
    private static CreditCard card = new CreditCard("Ivan Saladkou", BigDecimal.valueOf(500));

    private static ArrayList<CashMachine> producers;
    private static ArrayList<CashMachine> consumers;

    private static ArrayList<Runnable> runnables = new ArrayList<>();
    private static ArrayList<Thread> threads = new ArrayList<>();


    private static final BigDecimal DEFAULT_MONEY_AMOUNT = new BigDecimal(5000);
    private static final BigDecimal DEFAULT_CAPACITY = new BigDecimal(100000);


    public static void main(String[] args) {
        producers = createCashMachines(5);
        consumers = createCashMachines(3);
        initializeRunnables();
        initializeThreads();
        startAllThreads();
    }

    public static ArrayList<CashMachine> createCashMachines(int amount) {
        ArrayList cashMachines = new ArrayList();
        for (int i = 0; i < amount; i++) {
            CashMachine temporary = new CashMachine();
            temporary.setCapacity(DEFAULT_CAPACITY);
            temporary.setMoneyAmount(DEFAULT_MONEY_AMOUNT);
            cashMachines.add(temporary);
        }
        return cashMachines;
    }

    public static void initializeRunnables() {
        for (int i = 0; i < producers.size(); i++) {
            Runnable replenishment = new ReplenishmentRunnable(
                    producers.get(i), card, "moneyProducer" + i);
            runnables.add(replenishment);
        }
        for (int i = 0; i < consumers.size(); i++) {
            Runnable withdrawal = new WithdrawalRunnable(
                    consumers.get(i), card, "moneyConsumer" + i);
            runnables.add(withdrawal);
        }
    }

    private static void initializeThreads() {
        for (Runnable runnable : runnables) {
            threads.add(new Thread(runnable));
        }
    }

    private static void startAllThreads() {
        for (Thread thread : threads) {
            thread.start();
        }
    }


}
