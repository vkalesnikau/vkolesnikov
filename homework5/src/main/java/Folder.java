import java.util.ArrayList;

public class Folder {
    private String name;

    public String getName() {
        return name;
    }

    public Folder(String name) {
        this.name = name;
        files = new ArrayList<File>();
        folders = new ArrayList<Folder>();
    }

    private ArrayList<Folder> folders;
    private ArrayList<File> files;

    public ArrayList<File> getFiles() {
        return files;
    }

    public ArrayList<Folder> getFolders() {
        return folders;
    }

    public void addFile(File file) {
        files.add(file);
    }

    public void addFolder(Folder folder) {
        folders.add(folder);
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean containsFiles() {
        return !files.isEmpty();
    }

    public boolean containsFolders() {
        return !folders.isEmpty();
    }

    public boolean containsFolder(String folderName) {
        for (Folder folder : folders
        ) {
            if (folder.getName().equals(folderName)) {
                return true;
            }
        }
        return false;
    }

    public Folder getFolderByName(String folderName) {
        for (Folder folder : folders
        ) {
            if (folder.getName().equals(folderName)) {
                return folder;
            }
        }
        return null;
    }

    public boolean containsFile(String fullName) {
        for (File file : files
        ) {
            if (file.getFullName().equals(fullName)) {
                return true;
            }
        }
        return false;
    }

}