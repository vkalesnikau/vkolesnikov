package FirstTask;

public final class DigitSumGetter {
    private DigitSumGetter() {
    }
    public static Integer getDigitsSum(Integer number) {
        number = Math.abs(number);
        Integer sum = 0;
        String numberString = number.toString();
        char[] digitsCharArray = numberString.toCharArray();
        for (char digit : digitsCharArray) {
            sum += Character.getNumericValue(digit);
        }
        return sum;
    }


}
