import java.util.ArrayList;
import java.util.Arrays;

public class PathParser {

    private String path;

    public void setPath(String path) {
        this.path = path;
    }

    public ArrayList<String> getFoldersNamesFromPath() {
        String foldersString = getFoldersString();
        String[] foldersStringArray = foldersString.split("/");
        ArrayList<String> foldersList = new ArrayList<String>(Arrays.asList(foldersStringArray));
        return foldersList;
    }

    public String getFileNameFromPath() {
        int lastSlashIndex = path.lastIndexOf('/');
        String substring = path.substring(lastSlashIndex + 1);
        if (substring.contains(".")) {
            return substring;
        } else {
            return null;
        }
    }

    private String cutFileName(String path) {
        String fileSubstring = getFileNameFromPath();
        if (fileSubstring != null) {
            int fileNameIndex = path.indexOf(fileSubstring);
            return path.substring(0, fileNameIndex);
        } else {
            return path;
        }
    }

    private String cutRoot(String path) {
        int firstSlashIndex = path.indexOf('/');
        String substring = path.substring(firstSlashIndex + 1);
        return substring;
    }

    private String getFoldersString() {
        String str = cutFileName(path);
        return cutRoot(str);
    }


}
