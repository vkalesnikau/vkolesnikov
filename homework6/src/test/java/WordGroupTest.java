import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class WordGroupTest {

    private WordGroup wordGroup = new WordGroup("T");


    @Test
    public void testDistribute() {
        wordGroup.distribute("test");
        Occurrence occurrence = wordGroup.getOccurrence("test");
        Assert.assertEquals(occurrence, wordGroup.getOccurrence("test"));
    }

    @Test
    public void testDistributeExistingWord() {
        wordGroup.distribute("two");
        wordGroup.distribute("two");
        Occurrence expected = new Occurrence("two");
        expected.setAmount(2);
        Occurrence actual = wordGroup.getOccurrence("two");
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDistributeNull() throws IllegalArgumentException {
        wordGroup.distribute(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDistributeWrongWord() throws IllegalArgumentException {
        wordGroup.distribute(null);
    }


    @Test
    public void getOccurrence() {
        wordGroup.distribute("test");
        Occurrence actual = wordGroup.getOccurrence("test");
        Assert.assertEquals("test", actual.getWord());
    }

    @Test
    public void getNonExistentOccurrence() {
        assertNull(wordGroup.getOccurrence("nonexistent"));
    }

}
