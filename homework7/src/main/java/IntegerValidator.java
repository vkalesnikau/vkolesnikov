public class IntegerValidator implements Validator<Integer> {
    @Override
    public boolean validate(Integer input) {
        if (input >= 1 && input <= 10) {
            return true;
        } else {
            throw new ValidationFailedException("Number was not int the given range!");
        }
    }
}