import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        System.out.println("Please enter text in English");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        String noPunctuationText = TextEditor.deletePunctuation(text);
        ArrayList<String> words = TextEditor.convertToList(noPunctuationText);
        ArrayList<WordGroup> groups = Distributor.groupByFirstLetter(words);
        Collections.sort(groups);
        System.out.println(ListPrinter.print(groups));
    }
}