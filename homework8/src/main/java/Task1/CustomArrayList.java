package Task1;

import java.util.*;

public class CustomArrayList<T> implements List<T> {
    private static final int DEFAULT_CAPACITY = 10;
    private final int MAX_CAPACITY;
    private Object[] values;

    public CustomArrayList() {
        this(DEFAULT_CAPACITY);
    }

    public CustomArrayList(int maxCapacity) {
        if (maxCapacity < 0) {
            throw new IllegalArgumentException("List capacity can't be negative!");
        }
        MAX_CAPACITY = maxCapacity;
        values = new Object[0];
    }

    public CustomArrayList(T[] array) {
        this(array, array.length);
    }

    public CustomArrayList(T[] array, int maxCapacity) {
        this(maxCapacity);
        copyArrayToValues(array, Math.min(maxCapacity, array.length));
    }

    @Override
    public int size() {
        return values.length;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    public boolean isFull() {
        return size() == MAX_CAPACITY;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < size(); i++) {
            if (values[i].equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Object[] toArray() {
        return copyOfValues();
    }

    @Override
    public boolean add(T o) {
        if (checkOverflow(size() + 1)) {
            ResizeData(size() + 1);
            values[size() - 1] = o;
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        for (int index = 0; index < values.length; index++) {
            if (o.equals(values[index])) {
                removeInData(index);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        for (int i = 0; i < size(); i++) {
            if (c.contains(values[i])) {
                remove(values[i]);
                i--;
            }
        }
        return true;
    }

    @Override
    public void clear() {
        values = new Object[0];
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        Object[] arrayCollection = c.toArray();
        if (checkOverflow(size() + arrayCollection.length)) {
            return false;
        }
        for (int i = 0; i < arrayCollection.length; i++) {
            add((T) arrayCollection[i]);
        }
        return true;
    }

    @Override
    public T get(int index) {
        checkRange(index);
        return (T) values[index];
    }

    private void checkRange(int index) {
        if (index >= size() || index < 0) {
            throw new IndexOutOfBoundsException(
                    "The index was outside the bounds of the array!");
        }
    }

    private boolean checkOverflow(int size) {
        try {
            if (size > MAX_CAPACITY) {
                throw new FullListException("List is overflow!");
            }
            return true;
        } catch (FullListException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void ResizeData(int size) {
        Object[] newData = new Object[size];
        for (int i = 0; i < size(); i++) {
            newData[i] = values[i];
        }
        values = newData;
    }

    private void removeInData(int index) {
        Object[] newData = new Object[size() - 1];
        boolean isPassed = false;
        for (int i = 0; i < size(); i++) {
            if (i != index && !isPassed) {
                newData[i] = values[i];
            } else if (i != index && isPassed) {
                newData[i - 1] = values[i];
            } else if (i == index) {
                isPassed = true;
            }
        }
        values = newData;
    }

    private void copyArrayToValues(T[] array, int fillingOut) {
        values = new Object[fillingOut];
        for (int i = 0; i < fillingOut; i++) {
            values[i] = array[i];
        }
    }

    private Object[] copyOfValues() {
        Object[] array = new Object[size()];
        for (int i = 0; i < size(); i++) {
            array[i] = values[i];
        }
        return array;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        return false;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public Object set(int index, Object element) {
        return null;
    }

    @Override
    public void add(int index, Object element) {
    }

    @Override
    public T remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

}