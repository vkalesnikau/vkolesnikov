package FirstTask;

import java.util.Comparator;

public class DigitSumComparator implements Comparator {
    @Override
    public int compare(Object object1, Object object2) {
        Integer digitSum1 = DigitSumGetter.getDigitsSum((Integer) object1);
        Integer digitSum2 = DigitSumGetter.getDigitsSum((Integer) object2);
        return digitSum1.compareTo(digitSum2);
    }
}
