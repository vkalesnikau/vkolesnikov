import java.util.Scanner;

/**
 * Menu-class for Median class.
 */
public class Program {
    /**
     * Input point of the program.
     * There is 3 types of actions you can select.
     * Method includes while cycle, so you can select actions
     * until you select to exit.
     */
    public static void main(String[] args) {
        int action = 0;
        while (action != 3) {
            Scanner scanner = new Scanner(System.in);
            printSelectionInstructions();
            if (scanner.hasNextInt()) {
                action = scanner.nextInt();
            } else {
                action = 0;
            }
            switch (action) {
                case 1:
                    getIntArrayMedian();
                    break;
                case 2:
                    getDoubleMedian();
                    break;
                case 3:
                    break;
                default:
                    System.out.println("Incorrect input, please enter number 1-3");
            }
        }
    }

    /**
     * This method prints integer array median.
     * You should enter array to console dividing elements by space.
     */
    private static void getIntArrayMedian() {
        System.out.println("Please, enter integer array dividing elements by space");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        int[] array;
        try {
            array = convertStringToIntArray(input);
        } catch (Exception e) {
            printExceptionInstructions();
            return;
        }
        System.out.println("Median = " + Median.median(array));
    }

    /**
     * This method prints double array median.
     * You should enter array to console dividing elements by space.
     */
    static void getDoubleMedian() {
        System.out.println("Please, enter double array dividing elements by space");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        double[] array;
        try {
            array = convertStringToDoubleArray(input);
        } catch (Exception e) {
            printExceptionInstructions();
            return;
        }
        System.out.println("Median = " + Median.median(array));
    }

    /**
     * This method prints instructions for input.
     */
    private static void printInputInstructions() {
        System.out.println("Enter int or double array "
                + "dividing elements by space");
    }

    /**
     * This method prints instructions for selection.
     */
    static void printSelectionInstructions() {
        System.out.println("Please, select the action\n"
                + "1 - to calculate median of integer array\n"
                + "2 - to calculate median of double array\n"
                + "3 - to exit");
    }

    /**
     * This method prints instructions if an exception occurs.
     */
    private static void printExceptionInstructions() {
        System.out.println("Please verify your input\n"
                + "You should enter only numbers of required type");
    }

    /**
     * Method splits input string by space and transforms
     * received elements to integer.
     *
     * @param input - input string. Elements of array should
     *              be divided by space.
     * @return Method returns integer array.
     */
    static int[] convertStringToIntArray(String input) {
        String[] arguments = input.split(" ");
        int[] array = new int[arguments.length];
        for (int i = 0; i < arguments.length; i++) {
            array[i] = Integer.parseInt(arguments[i]);
        }
        return array;
    }

    /**
     * Method splits input string by space and transforms
     * received elements to double.
     *
     * @param input - input string. Elements of array should
     *              be divided by space.
     * @return Method returns double array.
     */
    static double[] convertStringToDoubleArray(String input) {
        String[] arguments = input.split(" ");
        double[] array = new double[arguments.length];
        for (int i = 0; i < arguments.length; i++) {
            array[i] = Double.parseDouble(arguments[i]);
        }
        return array;
    }
}
