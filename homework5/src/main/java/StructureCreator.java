import java.util.ArrayList;

public class StructureCreator {
    private PathParser parser;
    private Folder root;

    public StructureCreator(PathParser parser, Folder root) {
        this.parser = parser;
        this.root = root;
    }

    public void create() {
        createStructure(root, parser.getFoldersNamesFromPath());
    }

    private void createFile(Folder parentFolder) {
        String fullFilename = parser.getFileNameFromPath();
        if (fullFilename != null) {
            File file = new File(fullFilename);
            parentFolder.addFile(file);
        }
    }

    private void createStructure(Folder parentFolder, ArrayList<String> foldersNames) {
        if (!foldersNames.isEmpty()) {
            String tempName = foldersNames.get(0);
            foldersNames.remove(0);
            if (parentFolder.containsFolder(tempName)) {
                Folder childFolder = parentFolder.getFolderByName(tempName);
                selectAction(childFolder, foldersNames);
            } else {
                Folder childFolder = new Folder(tempName);
                parentFolder.addFolder(childFolder);
                selectAction(childFolder, foldersNames);
            }
        }
    }

    private void selectAction(Folder childFolder, ArrayList<String> foldersNames) {
        if (foldersNames.isEmpty()) {
            createFile(childFolder);
        } else {
            createStructure(childFolder, foldersNames);
        }
    }
}
