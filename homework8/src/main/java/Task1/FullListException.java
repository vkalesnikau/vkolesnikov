package Task1;

public class FullListException extends RuntimeException {
    public FullListException(final String message) {
        super(message);
    }
}

