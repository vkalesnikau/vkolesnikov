package Task.exceptions;

public class NoCashException extends RuntimeException {

    public NoCashException(String message) {
        super(message);
    }
}
