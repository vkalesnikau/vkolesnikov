import javafx.scene.layout.Background;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CreditCard extends Card{

    public CreditCard(String ownerName, String bankName) {
        super(ownerName, bankName);
    }

    public void addCash(BigDecimal cash) {
        balance = balance.add(cash);
    }

    public void withdrawCash(BigDecimal cash){
        if (cash.compareTo(BigDecimal.ZERO) == -1) {
            throw new NegativeCashWithdrawalException("You tried to withdraw negative count of cash");
        } else {
            balance = balance.add(cash.negate());
        }
    }
}
