package FirstTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public final class DigitSumSorter {
    private DigitSumSorter() {
    }

    public static void sort(Integer[] array) {
        ArrayList<Integer> sorted = new ArrayList<Integer>(Arrays.asList(array));
        Collections.sort(sorted, new DigitSumComparator());
        copyListToArray(sorted, array);
    }

    private static void copyListToArray(ArrayList<Integer> list, Integer[] array) {
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
    }
}