public class Occurrence {

    private String word;

    public Occurrence(String word) {
        this.word = word.toLowerCase();
        this.amount = 1;
    }

    public String getWord() {
        return word;
    }

    public void setAmount(int amount) {
        if (amount >= 0) {
            this.amount = amount;
        } else {
            throw new IllegalArgumentException(
                    "this parameter can't be negative");
        }
    }

    public int getAmount() {
        return amount;
    }

    private int amount;

    @Override
    public int hashCode() {
        return word.hashCode() * amount;
    }

    @Override
    public boolean equals(Object obj) {
        Occurrence occurrence = (Occurrence) obj;
        String word = occurrence.getWord();
        int amount = occurrence.getAmount();
        return (this.word.equals(word)
                && this.amount == amount);
    }

    @Override
    public String toString() {
        return word + " " + amount;
    }


}