package Task.card;

import Task.exceptions.InsufficientFundsException;

import java.math.BigDecimal;

public class DebitCard extends Card {
    public DebitCard(String ownerName, BigDecimal balance) {
        if (ownerName == null) {
            throw new NullPointerException("Please enter owner name");
        } else if (balance == null) {
            throw new NullPointerException("Please enter balance");
        } else if (balance.signum() == -1) {
            throw new IllegalArgumentException("Debit card"
                    + "can't have negative balance");
        } else {
            this.ownerName = ownerName;
            this.balance = balance;
        }
    }

    public DebitCard(String ownerName) {
        this(ownerName, BigDecimal.ZERO);
    }

    @Override
    public boolean withdraw(BigDecimal sum) {
        if (sum.signum() < 0) {
            throw new IllegalArgumentException("Sum can't be negative");
        } else if (balance.compareTo(sum) < 0) {
            throw new InsufficientFundsException("Attention, insufficient funds!");
        } else {
            balance = balance.subtract(sum);
            return true;
        }
    }
}

