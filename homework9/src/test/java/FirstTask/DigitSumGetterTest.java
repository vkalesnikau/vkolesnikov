package FirstTask;

import org.junit.Assert;
import org.junit.Test;

public class DigitSumGetterTest {

    @Test
    public void testGetDigits() {
        Assert.assertEquals((Integer) 6, DigitSumGetter.getDigitsSum(-123));
        Assert.assertEquals((Integer) 3, DigitSumGetter.getDigitsSum(12));
        Assert.assertEquals((Integer) 4, DigitSumGetter.getDigitsSum(22));
    }
}