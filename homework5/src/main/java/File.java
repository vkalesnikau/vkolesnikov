public class File {
    private String name;
    private String extension;

    public String getName() {
        return name;
    }

    public String getExtension() {
        return extension;
    }

    public File(String name, String extension) {
        this.name = name;
        this.extension = extension;
    }

    public File(String fullName) {
        int dotIndex = fullName.indexOf(".");
        this.name = fullName.substring(0, dotIndex);
        this.extension = fullName.substring(dotIndex + 1);
    }

    public String getFullName() {
        return name + "." + extension;
    }

}