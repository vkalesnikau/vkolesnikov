package Task.runnables;


import Task.CashMachine;
import Task.card.Card;

import java.math.BigDecimal;

public abstract class AtmRunnable implements Runnable {
    protected CashMachine atm;
    protected Card card;
    protected String atmName;


    private final BigDecimal WITHDRAWAL_LIMIT = new BigDecimal(0);
    private final BigDecimal REPLENISHMENT_LIMIT = new BigDecimal(1000);


    public AtmRunnable(CashMachine atm, Card card, String atmName) {
        this.atm = atm;
        this.card = card;
        this.atmName = atmName;
    }

    protected boolean isBalanceBetweenLimits() {
        if (card.getBalance().compareTo(WITHDRAWAL_LIMIT) != 1) {
            return false;
        } else if (card.getBalance().compareTo(REPLENISHMENT_LIMIT) == 1) {
            return false;
        } else {
            return true;
        }
    }

    protected abstract void printChangeBalanceMessage();

    protected void printFinishMessage() {
        System.out.println("Balance = " + card.getBalance()
                + "! " + atmName + " finished it's work");
    }

    protected void putThreadToSleep(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void run() {

    }
}