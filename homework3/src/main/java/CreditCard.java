import javafx.scene.layout.Background;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CreditCard {
    private String ownerName;
    private BigDecimal balance;

    public String getOwnerName() {
        return ownerName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public CreditCard(String ownerName, BigDecimal startBalance) {
        this.ownerName = ownerName;
        balance = startBalance;
    }
    public CreditCard(String ownerName) {
        this.ownerName = ownerName;
        balance = BigDecimal.ZERO;
    }

    public void addCash(BigDecimal cash) {
        balance = balance.add(cash);
    }

    public void withdrawCash(BigDecimal cash) throws NegativeCashWithdrawalException {
        if (cash.compareTo(BigDecimal.ZERO) == -1) {
            throw new NegativeCashWithdrawalException("You tried to withdraw negative count of cash");
        } else {
            balance = balance.add(cash.negate());
        }
    }
    public BigDecimal getBalanceInOtherCurrency(BigDecimal exchangeRates){
        BigDecimal balanceInOtherCurrency = balance.multiply(exchangeRates).setScale(2, RoundingMode.CEILING);
        return balanceInOtherCurrency;
    }
}
