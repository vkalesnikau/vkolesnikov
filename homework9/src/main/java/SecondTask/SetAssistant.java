package SecondTask;
import java.util.HashSet;

public class SetAssistant<T> {


    public HashSet<T> union(HashSet set1, HashSet set2) {
        HashSet result = new HashSet(set1);
        result.addAll(set2);
        return result;
    }

    public HashSet<T> intersection(HashSet set1, HashSet set2) {
        HashSet result = set1;
        result.retainAll(set2);
        return result;
    }

    public HashSet<T> minus(HashSet set1, HashSet set2) {
        HashSet result = set1;
        result.removeAll(set2);
        return result;
    }

    public HashSet<T> difference(HashSet set1, HashSet set2) {
        return minus(union(set1, set2), intersection(set1, set2));
    }


}
