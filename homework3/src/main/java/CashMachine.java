import org.junit.Assert;

import java.math.BigDecimal;
import java.util.Scanner;

public class CashMachine {
    public static BigDecimal usdExchangeRate = new BigDecimal(0.48);
    public static BigDecimal eurExchangeRate = new BigDecimal(0.44);
    public static BigDecimal rubExchangeRate = new BigDecimal(31.30);

    public static void main(String[] args) {

        try {
            showMessage("Enter your name or/and start card balance through white space to create a new card");
            Scanner consoleInput = new Scanner(System.in);
            String[] inputData = consoleInput.nextLine().split(" ");;

            while (inputData.length == 0) {
                showMessage("Incorrect entered data! Please repeat attempt");
                showMessage("Enter your name or/and start card balance through white space to create a new card");
                inputData = consoleInput.nextLine().split(" ");
            }
            String name = inputData[0];
            BigDecimal startBalance = null;
            if (inputData.length > 1) {
                startBalance = new BigDecimal(inputData[1]);
            }
            CreditCard creditCard;

            if (startBalance instanceof BigDecimal) {
                creditCard = new CreditCard(name, startBalance);
            } else {
                creditCard = new CreditCard(name);
            }
            int operationNumber = 0;
            while (operationNumber != 5) {
                operationNumber = 0;
                showMessage("Choose what kind of operation do you want to perform and enter number of operation in console");
                showMenu();
                operationNumber = consoleInput.nextInt();
                performOperation(operationNumber, creditCard);
            }
        } catch (NegativeCashWithdrawalException ex) {
            showErrorMessage(ex.getMessage());
        } catch (Exception ex1) {
            showErrorMessage(ex1.getMessage());
        }
    }

    private static void showErrorMessage(String errorMassage) {
        System.out.println(errorMassage);
    }

    private static void showMessage(String message) {
        System.out.println(message);
    }

    private static void showBalance(CreditCard creditCard) {
        System.out.println("Your exchange rates: " + creditCard.getBalance());
    }

    private static void performOperation(int operationNumber, CreditCard creditCard) throws NegativeCashWithdrawalException {
        if (operationNumber == 1) {
            showBalance(creditCard);
        } else if (operationNumber == 2) {
            showMessage("Enter cash into cash machine");
            Scanner consoleInput = new Scanner(System.in);

            BigDecimal cash = consoleInput.nextBigDecimal();
            creditCard.addCash(cash);
        } else if (operationNumber == 3) {
            showMessage("How mach cash do you want to withdraw");
            Scanner consoleInput = new Scanner(System.in);

            BigDecimal cashToWithdraw = consoleInput.nextBigDecimal();
            creditCard.withdrawCash(cashToWithdraw);
        } else if (operationNumber == 4) {
            showBalanceInOtherCurrencies(creditCard);
        } else if (operationNumber == 5){
            return;
        }
    }

    private static void showBalanceInOtherCurrencies(CreditCard creditCard) {
        showMessage("USD: " + creditCard.getBalanceInOtherCurrency(usdExchangeRate));
        showMessage("EUR: " + creditCard.getBalanceInOtherCurrency(eurExchangeRate));
        showMessage("RUB: " + creditCard.getBalanceInOtherCurrency(rubExchangeRate));
    }

    private static void showMenu() {
        System.out.println("1. Bring out balance on screen \n" +
                "2. Replenish account \n" +
                "3. Withdraw cash \n" +
                "4. Bring out balance in other currency \n" +
                "5. Exit");
    }

}
