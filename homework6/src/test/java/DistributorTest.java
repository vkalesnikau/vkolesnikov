import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class DistributorTest {

    private ArrayList<WordGroup> groups = new ArrayList<>();
    @Before
    public void createExpectedGroups(){

        WordGroup group1 = new WordGroup("T");
        group1.distribute("this");
        group1.distribute("test");
        groups.add(group1);

        WordGroup group2 = new WordGroup("I");
        group2.distribute("is");
        groups.add(group2);

        WordGroup group3 = new WordGroup("A");
        group3.distribute("a");
        groups.add(group3);
    }

    @Test
    public void testGroupByFirstLetter() {
        ArrayList<String> words = new ArrayList<>(
                Arrays.asList("This", "is", "a", "test")
        );
        Assert.assertEquals(groups,Distributor.groupByFirstLetter(words));
    }
}