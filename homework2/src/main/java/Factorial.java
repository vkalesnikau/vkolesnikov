/*
 * This class allows to find the factorial of a number using different loops types
 * @Author: Valentin Kolesnikov
 * @Version: 1.0
 */
public class Factorial {
    /*Field containing number*/
    private int number;

    /*Constructor - create new object*/
    public Factorial(int numberCount) {
        this.number = numberCount;
    }

    /*This method helps to calculate Factorial using loop for
     * @return factorial of number
     * */
    public int getFactorialByForLoop() {
        int result = 1;
        for (int i = 1; i <= this.number; i++) {
            result = result * i;
        }
        return result;
    }

    /*This method helps to calculate Factorial using loop while
     * @return factorial of number
     * */
    public int getFactorialByWhileLoop() {
        int result = 1;
        int i = 1;
        while (i <= this.number) {
            result = result * i;
            i++;
        }
        return result;
    }

    /*This method helps to calculate Factorial using loop do-while
     * @return factorial of number
     * */
    public int getFactorialByDoWhileLoop() {
        int i = 1;
        int result = 1;

        do {
            if (this.number != 0) {
                result = result * i;
                i++;
            }
        }
        while (i <= this.number);
        return result;
    }
}
