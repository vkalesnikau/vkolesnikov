package Task.card;

import java.math.BigDecimal;

public abstract class Card {

    protected String ownerName;

    protected BigDecimal balance;


    public BigDecimal getBalance() {
        return balance;
    }

    public boolean putMoneyOn(BigDecimal sum) {
        if (sum.signum() >= 0) {
            balance = balance.add(sum);
            return true;
        } else {
            throw new IllegalArgumentException("Sum can't be negative");
        }
    }

    public abstract boolean withdraw(BigDecimal sum);


}
