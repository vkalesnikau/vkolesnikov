import org.junit.Assert;

import java.math.BigDecimal;
import java.util.Scanner;

public class CashMachine {
    public static BigDecimal usdExchangeRate = new BigDecimal(0.48);
    public static BigDecimal eurExchangeRate = new BigDecimal(0.44);
    public static BigDecimal rubExchangeRate = new BigDecimal(31.30);

    public static void main(String[] args) {

        try {
            showMessage("Enter your name and bank name through white space to create a new card");
            Scanner consoleInput = new Scanner(System.in);
            String[] inputData = consoleInput.nextLine().split(" ");;

            while (inputData.length != 2) {
                showMessage("Incorrect entered data! Please repeat attempt");
                showMessage("Enter your name or/and start card balance through white space to create a new card");
                inputData = consoleInput.nextLine().split(" ");
            }
            String name = inputData[0];
            String bankName = inputData[1];
            showMessage("Enter what kind of card you want to create. choose from the options below");
            showMessage("1. Credit Card\n2. Debit Card");

            int cardID = consoleInput.nextInt();

            CreditCard creditCard;
            DebitCard debitCard;
            int operationNumber = 0;
            if (cardID == 1){
                creditCard = new CreditCard(name, bankName);
                while (operationNumber != 5) {
                    operationNumber = 0;
                    showMessage("Choose what kind of operation do you want to perform and enter number of operation in console");
                    showMenu();
                    operationNumber = consoleInput.nextInt();
                    performOperation(operationNumber, creditCard);
                }
            } else if (cardID == 2){
                debitCard = new DebitCard(name, bankName);
                while (operationNumber != 5) {
                    operationNumber = 0;
                    showMessage("Choose what kind of operation do you want to perform and enter number of operation in console");
                    showMenu();
                    operationNumber = consoleInput.nextInt();
                    performOperation(operationNumber, debitCard);
                }
            }
        } catch (NegativeCashWithdrawalException ex) {
            showErrorMessage(ex.getMessage());
        } catch (Exception ex1) {
            showErrorMessage(ex1.getMessage());
        }
    }

    private static void showErrorMessage(String errorMassage) {
        System.out.println(errorMassage);
    }

    private static void showMessage(String message) {
        System.out.println(message);
    }

    private static void showBalance(CreditCard creditCard) {
        System.out.println("Your exchange rates: " + creditCard.getBalance());
    }
    private static void showBalance(DebitCard debitCard) {
        System.out.println("Your exchange rates: " + debitCard.getBalance());
    }
    private static void performOperation(int operationNumber, DebitCard debitCard) throws NegativeCashWithdrawalException {
        if (operationNumber == 1) {
            showBalance(debitCard);
        } else if (operationNumber == 2) {
            showMessage("Enter cash into cash machine");
            Scanner consoleInput = new Scanner(System.in);

            BigDecimal cash = consoleInput.nextBigDecimal();
            debitCard.addCash(cash);
        } else if (operationNumber == 3) {
            showMessage("How mach cash do you want to withdraw");
            Scanner consoleInput = new Scanner(System.in);

            BigDecimal cashToWithdraw = consoleInput.nextBigDecimal();
            debitCard.withdrawCash(cashToWithdraw);
        } else if (operationNumber == 4) {
            return;
        }
    }
    private static void performOperation(int operationNumber, CreditCard creditCard) throws NegativeCashWithdrawalException {
        if (operationNumber == 1) {
            showBalance(creditCard);
        } else if (operationNumber == 2) {
            showMessage("Enter cash into cash machine");
            Scanner consoleInput = new Scanner(System.in);

            BigDecimal cash = consoleInput.nextBigDecimal();
            creditCard.addCash(cash);
        } else if (operationNumber == 3) {
            showMessage("How mach cash do you want to withdraw");
            Scanner consoleInput = new Scanner(System.in);

            BigDecimal cashToWithdraw = consoleInput.nextBigDecimal();
            creditCard.withdrawCash(cashToWithdraw);
        } else if (operationNumber == 4) {
            return;
        }
    }



    private static void showMenu() {
        System.out.println("1. Bring out balance on screen \n" +
                "2. Replenish account \n" +
                "3. Withdraw cash \n" +
                "4. Exit");
    }

}
