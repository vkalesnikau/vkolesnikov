package Task.exceptions;

public class CrowdedAtmException extends RuntimeException {

    public CrowdedAtmException(String message) {
        super(message);
    }
}
