package FirstTask;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter integer array dividing numbers by space");
        String input = scanner.nextLine();
        String[] numbersStringArray = input.split(" ");
        Integer[] array = new Integer[numbersStringArray.length];
        try {
            for (int i = 0; i < array.length; i++) {
                array[i] = Integer.parseInt(numbersStringArray[i]);
            }
            DigitSumSorter.sort(array);
            System.out.println("Sorted array:\n"
                    + Arrays.toString(array));
        } catch (NumberFormatException exception) {
            exception.printStackTrace();
        }
    }
}