package Task2;

import java.util.ArrayList;

public class ConstructionProject {

    private Brigade requiredBrigade;

    public ConstructionProject(Brigade requiredBrigade) {
        this.requiredBrigade = requiredBrigade;
    }

    private ArrayList<Brigade> getSuitableBrigades(ArrayList<Brigade> brigades) {
        ArrayList<Brigade> suitable = new ArrayList<>();
        for (Brigade brigade : brigades) {
            if (brigade.equalsOrBetter(requiredBrigade)) {
                suitable.add(brigade);
            }
        }
        return suitable;
    }

    public Brigade getSuitableBrigade(ArrayList<Brigade> brigades) {
        ArrayList<Brigade> variants = getSuitableBrigades(brigades);
        int minPrice = variants.get(0).getFinancialOffer();
        int index = 0;
        for (int i = 0; i < variants.size(); i++) {
            if (variants.get(i).getFinancialOffer() < minPrice) {
                minPrice = variants.get(i).getFinancialOffer();
                index = i;
            }
        }
        return variants.get(index);
    }

}
