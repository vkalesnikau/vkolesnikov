import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class CreditCardTest {
    public CreditCard creditCard;

    @Test
    public void testCheckBalance() {
        creditCard = new CreditCard("Mike");
        Assert.assertEquals(BigDecimal.ZERO, creditCard.getBalance());
    }

    @Test
    public void testAddCash() {
        creditCard = new CreditCard("Mike");
        creditCard.addCash(new BigDecimal(300));
        Assert.assertEquals(new BigDecimal(300), creditCard.getBalance());
    }

    @Test
    public void testWithdrawCash() throws NegativeCashWithdrawalException {
        creditCard = new CreditCard("Mike", new BigDecimal(300));
        creditCard.withdrawCash(new BigDecimal(100));
        Assert.assertEquals(new BigDecimal(200), creditCard.getBalance());
    }

    @Test
    public void testCheckBalanceInUsd() {
        creditCard = new CreditCard("Mike", new BigDecimal(500));
        Assert.assertEquals(new BigDecimal("240.00"), creditCard.getBalanceInOtherCurrency(CashMachine.usdExchangeRate));
    }
    @Test
    public void testCheckBalanceInEur() {
        creditCard = new CreditCard("Mike", new BigDecimal(500));
        Assert.assertEquals(new BigDecimal("220.01"), creditCard.getBalanceInOtherCurrency(CashMachine.eurExchangeRate));
    }

    @Test
    public void testCheckBalanceInRub() {
        creditCard = new CreditCard("Mike", new BigDecimal(500));
        Assert.assertEquals(new BigDecimal("15650.01"), creditCard.getBalanceInOtherCurrency(CashMachine.rubExchangeRate));
    }

    @Test
    public void testGetOwnerName() {
        creditCard = new CreditCard("Mike", new BigDecimal(500));
        Assert.assertEquals("Mike",  creditCard.getOwnerName());
    }

    @Test(expected = NegativeCashWithdrawalException.class)
    public void testWithdrawNegativeCount() throws NegativeCashWithdrawalException {
        creditCard = new CreditCard("Mike", new BigDecimal(500));
        creditCard.withdrawCash(new BigDecimal(-100));
    }


}