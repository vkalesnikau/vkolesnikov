import java.math.BigDecimal;

public abstract class Card {
    protected String ownerName;
    protected String bankName;
    protected BigDecimal balance;

    public BigDecimal getBalance() {
        return balance;
    }

    public Card(String ownerName, String bankName) {
        this.ownerName = ownerName;
        this.bankName = bankName;
        balance = BigDecimal.ZERO;
    }

    public abstract void addCash(BigDecimal cash);
    public abstract void withdrawCash(BigDecimal cash);
}
