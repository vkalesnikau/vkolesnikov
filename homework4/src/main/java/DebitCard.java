import java.math.BigDecimal;

public class DebitCard extends Card {

    public DebitCard(String ownerName, String bankName) {
        super(ownerName, bankName);
    }

    public void addCash(BigDecimal cash) {
        balance = balance.add(cash);
    }

    public void withdrawCash(BigDecimal cash){
        if (cash.compareTo(BigDecimal.ZERO) == -1) {
            throw new NegativeCashWithdrawalException("You tried to withdraw negative count of cash");
        } else if (cash.compareTo(balance) == 1) {
            throw new InsufficientFundsException("Insufficient funds");
        } else {
            balance = balance.add(cash.negate());
        }
    }

}
