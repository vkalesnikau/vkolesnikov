package SecondTask;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;

public class SetAssistantTest {
    private HashSet<String> set1 = new HashSet<>(Arrays.asList(new String[]{"A", "B"}));
    private HashSet<String> set2 = new HashSet<>(Arrays.asList(new String[]{"B", "C"}));

    private HashSet<String> union = new HashSet<>(Arrays.asList(new String[]{"A", "B", "C"}));
    private HashSet<String> intersection = new HashSet<>(Arrays.asList(new String[]{"B"}));
    private HashSet<String> minus = new HashSet<>(Arrays.asList(new String[]{"A"}));
    private HashSet<String> difference = new HashSet<>(Arrays.asList(new String[]{"A", "C"}));

    private SetAssistant<String> setAssistant = new SetAssistant<>();

    @Test
    public void testUnion() {
        Assert.assertEquals(union, setAssistant.union(set1, set2));
    }

    @Test
    public void testIntersection() {
        Assert.assertEquals(intersection, setAssistant.intersection(set1, set2));
    }

    @Test
    public void testMinus() {
        Assert.assertEquals(minus, setAssistant.minus(set1, set2));
    }

    @Test
    public void testDifference() {
        Assert.assertEquals(difference, setAssistant.difference(set1, set2));
    }
}
