import java.util.Scanner;


public class Program {

    public static void main(String[] args) {
        printSelectionInstructions();
        PathParser parser = new PathParser();
        Folder root = (Folder) Serializator.deserialize("testSerialize.ser");
        Printer printer = new Printer();
        StructureCreator creator = new StructureCreator(parser, root);
        String action = "";
        while (!action.equalsIgnoreCase("exit")) {
            Scanner scanner = new Scanner(System.in);
            action = scanner.nextLine();
            if (action.equalsIgnoreCase("print")) {
                System.out.println(printer.print(root));
            } else if (action.startsWith("root")) {
                parser.setPath(action);
                creator.create();
            } else if (action.equalsIgnoreCase("save")) {
                Serializator.serialize(root, "testSerialize.ser");
            } else if (action.equalsIgnoreCase("exit")) {
                break;
            } else {
                System.out.println("Please, check your input");
                printSelectionInstructions();
            }
        }
    }

    static void printSelectionInstructions() {
        System.out.println("To select action, please type\n"
                + "root/path - to add new structure\n"
                + "print - to print structure\n"
                + "save - to save structure to file\n"
                + "exit - to exit"
        );
    }


}
