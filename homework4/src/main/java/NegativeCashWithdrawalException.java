public class NegativeCashWithdrawalException extends RuntimeException {
    public NegativeCashWithdrawalException(String errorMessage) {
        super(errorMessage);
    }
}
