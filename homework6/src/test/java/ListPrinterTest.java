import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class ListPrinterTest {

    @Test
    public void testPrint() {
        String expected = "This is a test";
        ArrayList<String> list = new ArrayList<>(
                Arrays.asList("This", "is", "a", "test")
        );
        Assert.assertEquals(expected, ListPrinter.print(list));
    }
}