package Task;

import Task.card.Card;
import Task.exceptions.CrowdedAtmException;
import Task.exceptions.NoCashException;


import java.math.BigDecimal;

public class CashMachine {

    private BigDecimal capacity;
    private BigDecimal moneyAmount;

    public BigDecimal getCapacity() {
        return capacity;
    }

    public void setCapacity(BigDecimal capacity) {
        if (capacity.signum() <= 0) {
            throw new IllegalArgumentException("ATM capacity can't be non positive");
        } else {
            this.capacity = capacity;
        }
    }

    public BigDecimal getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(BigDecimal moneyAmount) {
        if (moneyAmount.signum() == -1) {
            throw new IllegalArgumentException("Money amount can't be negative");
        } else if (moneyAmount.compareTo(capacity) == 1) {
            throw new IllegalArgumentException("Money amount can't bigger than ATM capacity");
        } else {
            this.moneyAmount = moneyAmount;
        }
    }

    public BigDecimal getDifference() {
        BigDecimal capacity = getCapacity();
        BigDecimal moneyAmount = getMoneyAmount();
        BigDecimal difference = capacity.subtract(moneyAmount);
        return difference;
    }

    public CashMachine() {
        this.capacity = BigDecimal.ZERO;
        this.moneyAmount = BigDecimal.ZERO;
    }

    public synchronized boolean replenishAccount(Card card, BigDecimal sum) {
        if (card == null) {
            throw new NullPointerException("Please insert the card!");
        } else if (sum == null) {
            throw new NullPointerException("Please enter the sum!");
        } else if (getDifference().compareTo(sum) >= 0) {
            card.putMoneyOn(sum);
            setMoneyAmount(getMoneyAmount().add(sum));
            return true;
        } else {
            throw new CrowdedAtmException("ATM is crowded");
        }
    }

    public synchronized boolean withdrawMoney(Card card, BigDecimal sum) {
        if (card == null) {
            throw new NullPointerException("Please insert the card!");
        } else if (sum == null) {
            throw new NullPointerException("Please enter the sum!");
        } else if (getMoneyAmount().compareTo(sum) >= 0) {
            card.withdraw(sum);
            setMoneyAmount(getMoneyAmount().subtract(sum));
            return true;
        } else {
            throw new NoCashException("No cash in ATM");
        }
    }

}