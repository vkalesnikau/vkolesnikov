import org.apache.commons.lang3.StringUtils;

public class Printer {

    public StringBuilder print(Folder folder) {
        return getPrintingForm(folder, 0);
    }

    private StringBuilder getPrintingForm(Folder currentFolder, int tabsMultiplier) {
        String indentation = calculateIndentation(tabsMultiplier);
        String firstString = indentation + currentFolder.getName() + "/\n";
        StringBuilder printingForm = new StringBuilder(firstString);
        tabsMultiplier++;
        if (currentFolder.containsFiles()) {
            printingForm.append(getFilesPrintString(currentFolder, tabsMultiplier));
        }
        if (currentFolder.containsFolders()) {
            for (Folder inFolder : currentFolder.getFolders()
            ) {
                printingForm.append(getPrintingForm(inFolder, tabsMultiplier));
            }
        }
        return printingForm;
    }

    private String calculateIndentation(int tabsMultiplier) {
        return StringUtils.repeat("\t", tabsMultiplier);
    }

    private StringBuilder getFilesPrintString(Folder currentFolder, int tabsMultiplier) {
        StringBuilder printString = new StringBuilder();
        String indentation = calculateIndentation(tabsMultiplier);
        for (File file : currentFolder.getFiles()
        ) {
            printString.append(indentation + file.getFullName() + "\n");
        }
        return printString;
    }


}