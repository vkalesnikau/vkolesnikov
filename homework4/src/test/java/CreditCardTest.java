import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class CreditCardTest {
    public CreditCard creditCard;
    public DebitCard debitCard;

    @Test
    public void testCheckBalanceCreditCard() {
        creditCard = new CreditCard("Mike", "BelarusBank");
        Assert.assertEquals(BigDecimal.ZERO, creditCard.getBalance());
    }

    @Test
    public void testCheckBalanceDebitCard() {
        debitCard = new DebitCard("Mike", "BelarusBank");
        Assert.assertEquals(BigDecimal.ZERO, debitCard.getBalance());
    }

    @Test
    public void testAddCashCreditCard() {
        creditCard = new CreditCard("Mike", "BelarusBank");
        creditCard.addCash(new BigDecimal(300));
        Assert.assertEquals(new BigDecimal(300), creditCard.getBalance());
    }

    @Test
    public void testAddCashDebitCard() {
        debitCard = new DebitCard("Mike", "BelarusBank");
        debitCard.addCash(new BigDecimal(300));
        Assert.assertEquals(new BigDecimal(300), debitCard.getBalance());
    }

    @Test
    public void testWithdrawCashCreditCard() {
        creditCard = new CreditCard("Mike", "BelarusBank");
        creditCard.addCash(new BigDecimal(300));
        creditCard.withdrawCash(new BigDecimal(100));
        Assert.assertEquals(new BigDecimal(200), creditCard.getBalance());
    }

    @Test
    public void testWithdrawCashDebitCard() {
        debitCard = new DebitCard("Mike", "BelarusBank");
        debitCard.addCash(new BigDecimal(300));
        debitCard.withdrawCash(new BigDecimal(100));
        Assert.assertEquals(new BigDecimal(200), debitCard.getBalance());
    }

    @Test(expected = NegativeCashWithdrawalException.class)
    public void testWithdrawNegativeCountCreditCard() {
        creditCard = new CreditCard("Mike","BelarusBank");
        creditCard.withdrawCash(new BigDecimal(-100));
    }

    @Test(expected = NegativeCashWithdrawalException.class)
    public void testWithdrawNegativeCountDebitCard() {
        debitCard = new DebitCard("Mike","BelarusBank");
        debitCard.withdrawCash(new BigDecimal(-100));
    }

    @Test(expected = InsufficientFundsException.class)
    public void testWithdrawNegativeCount() throws NegativeCashWithdrawalException {
        debitCard = new DebitCard("Mike","Bel");
        debitCard.addCash(new BigDecimal(300));
        debitCard.withdrawCash(new BigDecimal(500));
    }

}