package Task1;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import java.util.ArrayList;

public class CustomArrayListTest {

    private final int NEGATIVE_CAPACITY = -1;
    private final int ONE = 1;
    private final int TWO = 2;
    private final int THREE = 3;
    private final int FIVE = 5;
    private final String[] DEFAULT_ARRAY = new String[]{"1", "2", "3"};
    private CustomArrayList<String> customArrayListInitDefault;
    private CustomArrayList<String> customArrayListInitByCapacity;
    private CustomArrayList<String> customArrayListInitByArray;
    private CustomArrayList<String> customArrayListInitByArrayAndCapacity;

    @Before
    public void init() {
        customArrayListInitByArrayAndCapacity = new CustomArrayList<>(DEFAULT_ARRAY, FIVE);
        customArrayListInitByArray = new CustomArrayList<>(DEFAULT_ARRAY);
        customArrayListInitByCapacity = new CustomArrayList<>(THREE);
        customArrayListInitDefault = new CustomArrayList<>();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorNegativeCapacity() {
        CustomArrayList<String> customArrayList = new CustomArrayList<>(NEGATIVE_CAPACITY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorNegativeCapacityInitWithArray() {
        CustomArrayList<String> customArrayList = new CustomArrayList<>(DEFAULT_ARRAY, NEGATIVE_CAPACITY);
    }

    @Test
    public void testConstructorArrayAndCapacity() {
        CustomArrayList<String> customArrayList = new CustomArrayList<>(DEFAULT_ARRAY, 8);
        Assert.assertEquals(3, customArrayList.size());
    }

    @Test
    public void testIsEmpty() {
        Assert.assertEquals(true, customArrayListInitDefault.isEmpty());
        Assert.assertEquals(false, customArrayListInitByArrayAndCapacity.isEmpty());
    }

    @Test
    public void testContains() {
        Assert.assertEquals(true, customArrayListInitByArray.contains("1"));
        Assert.assertEquals(false, customArrayListInitByArray.contains("4"));
    }

    @Test
    public void testToArray() {
        Object[] array = customArrayListInitByArrayAndCapacity.toArray();
        Assert.assertEquals(THREE, array.length);
    }

    @Test
    public void testIsFull() {
        Assert.assertEquals(false, customArrayListInitDefault.isFull());
        Assert.assertEquals(false, customArrayListInitByCapacity.isFull());
        Assert.assertEquals(true, customArrayListInitByArray.isFull());
        Assert.assertEquals(false, customArrayListInitByArrayAndCapacity.isFull());
    }

    @Test
    public void testAdd() {
        String string = "string";
        customArrayListInitByArrayAndCapacity.add(string);
        Assert.assertEquals(4, customArrayListInitByArrayAndCapacity.size());
        customArrayListInitByArrayAndCapacity.add(string);
        Assert.assertEquals(FIVE, customArrayListInitByArrayAndCapacity.size());
        Assert.assertEquals(true, customArrayListInitByArrayAndCapacity.isFull());
        customArrayListInitByArrayAndCapacity.add(string);
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(THREE, customArrayListInitByArrayAndCapacity.size());
        customArrayListInitByArrayAndCapacity.remove("1");
        Assert.assertEquals(TWO, customArrayListInitByArrayAndCapacity.size());
        customArrayListInitByArrayAndCapacity.remove("1");
        Assert.assertEquals(TWO, customArrayListInitByArrayAndCapacity.size());
        customArrayListInitByArrayAndCapacity.remove("2");
        Assert.assertEquals(ONE, customArrayListInitByArrayAndCapacity.size());
    }

    @Test
    public void testRemoveAll() {
        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add("2");
        arrayList.add("3");
        customArrayListInitByArrayAndCapacity.removeAll(arrayList);
        Assert.assertEquals(ONE, customArrayListInitByArrayAndCapacity.size());
        customArrayListInitByArrayAndCapacity.add("2");
        Assert.assertEquals(TWO, customArrayListInitByArrayAndCapacity.size());
        arrayList.add("1");
        customArrayListInitByArrayAndCapacity.removeAll(arrayList);
        Assert.assertEquals(0, customArrayListInitByArrayAndCapacity.size());
    }

    @Test
    public void testClear() {
        customArrayListInitByArrayAndCapacity.clear();
        Assert.assertEquals(true, customArrayListInitByArrayAndCapacity.isEmpty());
    }

    @Test
    public void testGet() {
        String string = customArrayListInitByArrayAndCapacity.get(ONE);
        Assert.assertEquals(string, "2");
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetOutOfBounds() {
        String string = customArrayListInitByArrayAndCapacity.get(10);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetOutOfBoundsNrgative() {
        String string = customArrayListInitByArrayAndCapacity.get(NEGATIVE_CAPACITY);
    }
}