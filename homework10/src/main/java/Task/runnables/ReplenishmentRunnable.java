package Task.runnables;

import Task.CashMachine;
import Task.card.Card;


import java.math.BigDecimal;

public class ReplenishmentRunnable extends AtmRunnable {

    private final int SLEEP_TIME = 2000;
    private final BigDecimal REPLENISHMENT_SUM = new BigDecimal(10);

    public ReplenishmentRunnable(CashMachine atm, Card card, String atmName) {
        super(atm, card, atmName);
    }

    @Override
    protected void printChangeBalanceMessage() {
        System.out.println("Balance was replenished by " + atmName
                + ", balance = " + card.getBalance() + "$");
    }


    @Override
    public void run() {
        do {
            atm.replenishAccount(card, REPLENISHMENT_SUM);
            printChangeBalanceMessage();
            putThreadToSleep(SLEEP_TIME);
        }
        while (isBalanceBetweenLimits());
        printFinishMessage();

    }
}