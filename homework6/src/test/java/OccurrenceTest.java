import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class OccurrenceTest {

    private Occurrence occurrence = new Occurrence("test");

    @Test
    public void testSetAmount() {
        int expectedAmount = occurrence.getAmount() + 1;
        occurrence.setAmount(expectedAmount);
        Assert.assertEquals(expectedAmount, occurrence.getAmount());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetWrongAmount() throws IllegalArgumentException {
        occurrence.setAmount(-1);
    }

}