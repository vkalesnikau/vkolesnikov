public class NegativeCashWithdrawalException extends Exception {
    public NegativeCashWithdrawalException(String errorMessage) {
        super(errorMessage);
    }
}
