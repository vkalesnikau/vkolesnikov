import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class PathParserTest {

    private PathParser parser= new PathParser();
    private String path = "root/folder1/folder2/test.txt";
    private String pathWithoutFile ="root/folder1/folder2";



    @Test
    public void testGetFoldersNamesFromPath() {
        parser.setPath(path);
        ArrayList<String> foldersNames = new ArrayList<String>();
        foldersNames.add("folder1");
        foldersNames.add("folder2");
        assertEquals(foldersNames,parser.getFoldersNamesFromPath());
    }

    @Test
    public void testGetFileNameFromPath() {
        parser.setPath(path);
        String fileName ="test.txt";
        assertEquals(fileName,parser.getFileNameFromPath());
    }

    @Test
    public void testGetFoldersNamesFromPathWithoutFile() {
        parser.setPath(pathWithoutFile);
        ArrayList<String> foldersNames = new ArrayList<String>();
        foldersNames.add("folder1");
        foldersNames.add("folder2");
        assertEquals(foldersNames,parser.getFoldersNamesFromPath());
    }
    @Test
    public void testGetFileNameFromPathWithoutFile() {
        parser.setPath(pathWithoutFile);
        assertEquals(null,parser.getFileNameFromPath());
    }
}