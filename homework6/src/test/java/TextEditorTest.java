import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class TextEditorTest {

    @Test
    public void testDeletePunctuation() {
        String before = "This, string;$% contains- no :(punctuation)!";
        String after = "This string contains no punctuation";
        Assert.assertEquals(after, TextEditor.deletePunctuation(before));
    }

    @Test
    public void testConvertToList() {
        String test = "This is a test string";
        ArrayList<String> expectedList = new ArrayList<String>(
                Arrays.asList("This", "is", "a", "test", "string"));
        Assert.assertEquals(expectedList, TextEditor.convertToList(test));
    }


}