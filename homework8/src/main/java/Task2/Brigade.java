package Task2;

import java.util.ArrayList;

public class Brigade {
    private ArrayList<Worker> workers;

    public int getFinancialOffer() {
        return financialOffer;
    }

    private int financialOffer;

    public Brigade(ArrayList<Worker> workers, int financialOffer) {
        this.workers = workers;
        this.financialOffer = financialOffer;
    }

    public boolean equalsOrBetter(Brigade other) {
        return (hasNecessaryWorkers(other) && financialOffer <= other.financialOffer);
    }

    private boolean hasNecessaryWorkers(Brigade other) {
        ArrayList<Skill> skills = other.getSkills();
        for (Skill skill : skills) {
            int available = this.getWorkersAmountBySkill(skill);
            int necessary = other.getWorkersAmountBySkill(skill);
            if (available < necessary) {
                return false;
            }
        }
        return true;
    }


    private int getWorkersAmountBySkill(Skill skill) {
        int amount = 0;
        for (Worker worker : workers) {
            if (worker.hasThisSkill(skill)) {
                amount++;
            }
        }
        return amount;
    }

    private ArrayList<Skill> getSkills() {
        ArrayList<Skill> skills = new ArrayList<>();
        for (Worker worker : workers) {
            for (Skill skill : worker.skills) {
                if (!skills.contains(skill)) {
                    skills.add(skill);
                }
            }
        }
        return skills;
    }

    @Override
    public String toString() {
        StringBuilder brigadeString = new StringBuilder();
        int counter = 1;
        for (Worker worker : workers) {
            brigadeString.append(counter + ". " + worker.toString() + "\n");
            counter++;
        }
        brigadeString.append("Financial Offer: " + getFinancialOffer());
        return brigadeString.toString();
    }
}
