package Task2;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class WorkerTest {

    private Worker carpenter = new Worker(new ArrayList(Arrays.asList(Skill.CARPENTER)));
    private Worker carpenterElectrician = new Worker(new ArrayList(Arrays.asList(Skill.CARPENTER, Skill.ELECTRICIAN)));

    @Test
    public void testEqualsOrBetterFalse() {
        Assert.assertFalse(carpenter.equalsOrBetter(carpenterElectrician));
    }

    @Test
    public void testEqualsOrBetterTrue() {
        Assert.assertTrue(carpenterElectrician.equalsOrBetter(carpenter));
    }

    @Test
    public void testHasThisSkillTrue() {
        Assert.assertTrue(carpenter.hasThisSkill(Skill.CARPENTER));
    }

    @Test
    public void testHasThisSkillFalse() {
        Assert.assertFalse(carpenterElectrician.hasThisSkill(Skill.MASON));
    }
}